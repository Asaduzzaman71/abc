<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\user\post;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([
            
            'title' => 'required|max:255',
            'subtitle' => 'required|max:100',
            'slug'=>'required|max:100',
            'body' => 'required|max:1000',
            ]);

        if($request->hasFile('image')){
          $image= $request->file('image'); 
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='uploads/images/';
            $image_url=$upload_path.$image_full_name; 
            $image->move($upload_path,$image_full_name);            
            //Image::make($image)->resize(300, 300)->save( public_path( $image_url ) );
            $post->image=$image_url;
            
        }
            $post=new post();
            $post->title=$request->title;
            $post->subtitle=$request->subtitle;
            $post->slug=$request->slug;
          //  $post->status=$request->status;
            $post->body=$request->body;
            //$post->image=$request->image;
           /* $post->posted_by=$request->posted_by;
            $post->like=$request->like;
            $post->dislike=$request->dislike;*/
            $post->save();
            return redirect(route('post.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
