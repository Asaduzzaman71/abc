<?php

//user controller
Route::group(['namespace'=>'User'],function(){

	Route::get('/','HomeController@index');
	Route::get('/post','HomeController@post');

});







//Admin routes


Route::group(['namespace'=>'Admin'],function(){
	    Route::get('admin/home','HomeController@index');
	    //post routes
		Route::resource('admin/post','PostController');
		//tag routes
		Route::resource('admin/tag','TagController');
		//category routes
		Route::resource('admin/category','CategoryController');
		//User routes
		Route::resource('admin/user','UserController');
	});
