<!DOCTYPE html>
<html lang="en">

<head>
 @include('user.partials._header')

</head>

<body>

  <!-- Navigation -->
  @include('user.partials._nav')
  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('{{asset('user/img/home-bg.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  @yield('content')
  <hr>

  <!-- Footer -->
  @include('user.partials._footer')
  @include('user.partials._javascript')



</body>

</html>
