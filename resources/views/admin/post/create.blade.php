@extends('admin.layout')
@section('content')

    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">


          <div class="card card-outline card-info">
            

            <div class="card-header">
              <h3 class="card-title">
                Bootstrap WYSIHTML5
                <small>Simple and fast</small>
              </h3>
            </div>

          <div  class="container">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('post.store')}}" method="post">
                @csrf
                <div class="card-body">
                  <div class="row">
                     <div class="col-md-6">
                     <div class="form-group">
                    <label for="title">Post title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Post Title">
                  </div>
                  <div class="form-group">
                    <label for="title">Post Subtitle</label>
                    <input type="text" name="subtitle" class="form-control" id="subtitle" placeholder="Post subtitle">
                  </div>
                  <div class="form-group">
                    <label for="slug">Post Slug</label>
                    <input type="text" name="slug" class="form-control" id="slug" placeholder="Post slug">
                  </div>
                    

                  </div>
                <div class="col-md-6">
                     <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="image">
                        <label class="custom-file-label" for="image">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="image">Upload</span>
                      </div>
                    </div>
                  </div>
                  <br>
                    <br>


                  <div class="form-check">
                    <input type="checkbox" name="status" class="form-check-input" id="status">
                    <label class="form-check-label"  for="status">Publish</label>
                  </div>
              </div>
                    
                  </div>
                 
                  
                 
                  
                 

              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                
              </div>
              
              <div class="mb-3">
                <textarea class="textarea"name="body" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              <p class="text-sm mb-0">
                Editor <a href="https://github.com/bootstrap-wysiwyg/bootstrap3-wysiwyg">Documentation and license
                information.</a>
              </p>
              

              <div class="form-group">
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
             

                <!-- tools box -->
              
             </form>
           </div>
          
              
              
          </div>
          
          
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  
@endsection