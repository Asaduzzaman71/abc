@extends('admin.layout')
@section('css')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('content')
<div class="card">
            <div class="card-header">
              <div class="row">
              <div class="col-md-5">
                 <h3 class="card-title">Category table</h3>
              </div>
             
              <div class='col-md-offset-5' > <a class='btn btn-success' href="{{route('category.create')}}">Add category</a></div>

             
              </div>
            </div>
	 <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl.No</th>
                  <th>Category Name</th>
                  <th>Slug</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($categories as $category)
                <tr>
                  <td>{{$category->id}}</td>
                  <td>{{$category->name}}</td>
                  <td>{{$category->slug}}</td>
                  <td> </td>
                  <td>X</td>
                </tr>
                @endforeach
             
                </tbody>
                <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot>
              </table>
            </div>
        </div>
  @section('script')
            <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
            <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  @endsection
@endsection