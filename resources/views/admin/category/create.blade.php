@extends('admin.layout')
@section('content')

    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content" style="margin-left: 5px">
     

          <div  class="container">
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('category.store')}}" method="post">
                @csrf
                <div class="card-body">
                  <div class="row">
                  	<div class="col-md-3"></div>
                  	<div class="col-md-offset-3 col-md-6">
                  		<div class="form-group">
                    	<label for="title">Category title</label>
                    	<input type="text" name="name" class="form-control" id="name" placeholder="Category Title">
                  	</div>
                 
                  	<div class="form-group">
                    	<label for="slug">Category Slug</label>
                    	<input type="text" name="slug" class="form-control" id="slug" placeholder="Category slug">
                  	</div>
          
                 
             
              		<div class="form-group">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                      <a href="{{route('category.index')}}" class="btn btn-warning">Back</a>
              		</div>
                  	</div>
                  	

                  </div>
                     
            </div>
             

                <!-- tools box -->
              
             </form>
           
          
              
              
          </div>
          
          
          </div>
        </div>
        <!-- /.col-->
      </div>
  </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  
@endsection