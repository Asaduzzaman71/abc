<!DOCTYPE html>
<html lang="en">

<head>
 @include('user.partials._head')

</head>

<body>

  <!-- Navigation -->
  @include('user.partials._nav')
  
  <!-- Page Header -->
  @include('user.partials._header')

  <!-- Main Content -->
  @yield('content')
  <hr>

  <!-- Footer -->
  @include('user.partials._footer')
  @include('user.partials._javascript')



</body>

</html>
